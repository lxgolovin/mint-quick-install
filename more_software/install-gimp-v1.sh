#!/bin/bash

sudo add-apt-repository ppa:otto-kesselgulasch/gimp -y
sudo apt-get update
sudo apt install gimp -y

echo "################################################################"
echo "###################     GIMP installed    ######################"
echo "################################################################"
