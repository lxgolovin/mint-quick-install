#!/bin/bash

sudo add-apt-repository ppa:clipgrab-team/ppa -y
sudo apt-get update
sudo apt-get install clipgrab -y

echo "################################################################"
echo "###################    T H E   E N D      ######################"
echo "################################################################"
